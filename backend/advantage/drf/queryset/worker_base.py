from abc import ABC, abstractmethod


class WorkerBase(ABC):
    type_name = None
    serializer = None

    def __init__(self, field, worker_data):
        self.field = field
        self.data = None
        self.normalize_data(worker_data)

    def normalize_data(self, data):
        sr = self.serializer(data=data)
        if sr.is_valid(raise_exception=True):
            self.data = sr.validated_data
        else:
            self.data = {}