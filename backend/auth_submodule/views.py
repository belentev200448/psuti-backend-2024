import os

from django.contrib import auth
from django.contrib.auth import get_user_model
from django.shortcuts import render
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from advantage.drf.utils import get_object_or_none
from auth_submodule.models import User
from auth_submodule.oauth.google import get_user_data
from auth_submodule.serializers import UserRegistrationSerializer, TokenAuthOutputSerializer, TokenAuthInputSerializer, \
    EmailConfirmCodeSerializer, PasswordResetEmailSerializer, UserDataSerializer, PasswordResetCodeSerializer
from auth_submodule.tfa import PasswordResetController
from auth_submodule.tfa.confirm_email import ConfirmEmailController
from auth_submodule.tfa.views import TFAViewSetGeneric
from rest_framework.request import Request
from advantage.drf.queryset.serializers import BlankSerializer
from advantage.drf.viewsets import SmartGenericViewSet
from advantage.drf.decorators import smart_action
from auth_submodule.utils import get_user_token
from advantage.recaptcha.main import Reasons
from backend.recaptcha import RecaptchaVirtualSpaceGeneric, RecaptchaParamsSerializer


class EmailAuthViewSet(TFAViewSetGeneric):
    auth_controller = ConfirmEmailController
    auth_identifier_field = "email"

    @smart_action(methods=['post'], detail=False)
    def login(self,
              request: Request,
              body: TokenAuthInputSerializer,
              params: RecaptchaParamsSerializer) -> TokenAuthOutputSerializer:
        """
        Recaptcha action: EmailAuthViewSet__login
        """
        def protected_view(request, body):
            user = auth.authenticate(
                email=body.validated_data.get('email'),
                password=body.validated_data.get('password')
            )
            if user is None:
                raise ValidationError("User not found")
            return get_user_token(user)

        recaptcha_virtual_space = RecaptchaVirtualSpaceGeneric(
            platform=params.validated_data['platform'],
            token=params.validated_data['token'],
            action='EmailAuthViewSet__login',
            on_success=[Reasons.CORRECT_PASSWORD],
            on_error=[Reasons.INCORRECT_PASSWORD],
            view=protected_view,
        )
        response = recaptcha_virtual_space(request, body)
        return response

    @smart_action(methods=['post'], detail=False)
    def register(self,
                 request: Request,
                 body: UserRegistrationSerializer,
                 params: RecaptchaParamsSerializer) -> TokenAuthOutputSerializer:
        """
        Recaptcha action: EmailAuthViewSet__register
        """
        super_send_code = super().send_code

        def protected_view(request, body):
            user = body.save()
            auth_identifier = body.validated_data['email']
            super_send_code(request, auth_identifier)
            return get_user_token(user)

        recaptcha_virtual_space = RecaptchaVirtualSpaceGeneric(
            platform=params.validated_data['platform'],
            token=params.validated_data['token'],
            action='EmailAuthViewSet__register',
            on_success=[Reasons.REASON_UNSPECIFIED],
            on_error=[Reasons.REASON_UNSPECIFIED],
            view=protected_view,
        )
        response = recaptcha_virtual_space(request, body)
        return response


class EmailConfirmViewSet(TFAViewSetGeneric):
    auth_controller = ConfirmEmailController
    auth_identifier_field = "email"
    permission_classes = [IsAuthenticated]

    @smart_action(methods=['post'], detail=False)
    def send_confirm_code(self,
                          request: Request,
                          params: RecaptchaParamsSerializer) -> BlankSerializer:
        """
        Recaptcha action: EmailConfirmViewSet__send_confirm_code
        """
        super_send_code = super().send_code

        def protected_view(request):
            if request.user.is_verified:
                raise ValidationError('user already verified email')

            auth_identifier = request.user.email
            super_send_code(request, auth_identifier)

        recaptcha_virtual_space = RecaptchaVirtualSpaceGeneric(
            platform=params.validated_data['platform'],
            token=params.validated_data['token'],
            action='EmailConfirmViewSet__send_confirm_code',
            on_success=[Reasons.INITIATED_TWO_FACTOR],
            on_error=[Reasons.INITIATED_TWO_FACTOR],
            view=protected_view,
        )
        response = recaptcha_virtual_space(request)
        return response

    @smart_action(methods=['post'], detail=False)
    def confirm_code(self,
                     request: Request,
                     body: EmailConfirmCodeSerializer,
                     params: RecaptchaParamsSerializer) -> BlankSerializer:
        """
        Recaptcha action: EmailConfirmViewSet__confirm_code
        """
        super_validate = super().validate

        def protected_view(request, body):
            if request.user.is_verified:
                raise ValidationError('user already verified email')

            code = body.validated_data['code']
            email = request.user.email
            super_validate(request, email, code)
            user = request.user
            user.is_verified = True
            user.save()

        recaptcha_virtual_space = RecaptchaVirtualSpaceGeneric(
            platform=params.validated_data['platform'],
            token=params.validated_data['token'],
            action='EmailConfirmViewSet__confirm_code',
            on_success=[Reasons.PASSED_TWO_FACTOR],
            on_error=[Reasons.FAILED_TWO_FACTOR],
            view=protected_view,
        )
        response = recaptcha_virtual_space(request, body)
        return response


class PasswordResetViewSet(TFAViewSetGeneric):
    auth_controller = PasswordResetController
    auth_identifier_field = "email"

    @smart_action(methods=['post'], detail=False)
    def send_reset_code(self,
                        request: Request,
                        body: PasswordResetEmailSerializer,
                        params: RecaptchaParamsSerializer) -> BlankSerializer:
        """
        Recaptcha action: PasswordResetViewSet__send_reset_code
        """
        super_send_code = super().send_code

        def protected_view(request, body):
            auth_identifier = body.validated_data['email']
            user_obj = get_object_or_none(User, email=auth_identifier)
            if not user_obj:
                raise ValidationError('User with this email does not exist')
            super_send_code(request, auth_identifier)

        recaptcha_virtual_space = RecaptchaVirtualSpaceGeneric(
            platform=params.validated_data['platform'],
            token=params.validated_data['token'],
            action='PasswordResetViewSet__send_reset_code',
            on_success=[Reasons.INITIATED_TWO_FACTOR],
            on_error=[Reasons.INITIATED_TWO_FACTOR],
            view=protected_view,
        )
        response = recaptcha_virtual_space(request, body)
        return response

    @smart_action(methods=['post'], detail=False)
    def reset_password(self,
                       request: Request,
                       body: PasswordResetCodeSerializer,
                       params: RecaptchaParamsSerializer) -> TokenAuthOutputSerializer:
        """
        Recaptcha action: PasswordResetViewSet__reset_password
        """
        super_validate = super().validate

        def protected_view(request, body):
            code = body.validated_data['code']
            email = body.validated_data['email']
            super_validate(request, email, code)
            user_obj = get_object_or_none(User, email=email)
            if not user_obj:
                raise ValidationError('User with this email does not exist')
            user_obj.is_verified = True
            user_obj.set_password(body.validated_data['password'])
            user_obj.save()
            return get_user_token(user_obj)

        recaptcha_virtual_space = RecaptchaVirtualSpaceGeneric(
            platform=params.validated_data['platform'],
            token=params.validated_data['token'],
            action='PasswordResetViewSet__reset_password',
            on_success=[Reasons.PASSED_TWO_FACTOR],
            on_error=[Reasons.FAILED_TWO_FACTOR],
            view=protected_view,
        )
        response = recaptcha_virtual_space(request, body)
        return response


class UserData(SmartGenericViewSet):
    permission_classes = [IsAuthenticated]

    @smart_action(methods=['GET'], detail=False)
    def is_authenticated(self, request: Request) -> BlankSerializer:
        return

    @smart_action(methods=['GET'], detail=False)
    def user_data(self, request: Request) -> UserDataSerializer:
        return request.user


def recaptcha(request):
    context = {
        'recaptcha_token': os.environ.get('recaptcha_web_key')
    }
    return render(request, 'recaptcha.html', context)
