from copy import copy

from rest_framework.serializers import Serializer, ModelSerializer
from colorfield.fields import ColorField as DjangoColorField
from colorfield.serializers import ColorField as DRFColorField
from translation.fields import to_attribute
from translation.utils import get_translated_field
from django.conf import settings


class SmartSerializer(Serializer):
    pass


class SmartModelSerializer(ModelSerializer):
    serializer_field_mapping = ModelSerializer.serializer_field_mapping
    serializer_field_mapping[DjangoColorField] = DRFColorField

    def build_field(self, field_name, info, model_class, nested_depth):
        super_build_field = super().build_field

        translated_field_obj = get_translated_field(model_class, field_name)
        # handle translation field
        if not translated_field_obj:
            return super_build_field(field_name, info, model_class, nested_depth)
        translated_field_name = to_attribute(field_name)
        return super_build_field(translated_field_name, info, model_class, nested_depth)

    def get_field_names(self, declared_fields, info):
        field_names = super().get_field_names(declared_fields, info)

        detach_translated_fields = getattr(self.Meta, 'detach_translated_fields', False)
        if detach_translated_fields:
            model = getattr(self.Meta, 'model', None)
            detached_field_names = list(field_names)
            for field_name in field_names:
                translated_field_obj = get_translated_field(model, field_name)
                if not translated_field_obj:
                    continue
                detached_field_names.remove(field_name)
                for lang_code, lang_name in settings.LANGUAGES + [settings.SRC_LANGUAGE]:
                    detached_field_names.append(to_attribute(field_name, language_code=lang_code))
            field_names = tuple(detached_field_names)
        return field_names

