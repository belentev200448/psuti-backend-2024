from django.contrib.auth import get_user_model
from django.utils.functional import SimpleLazyObject
from rest_framework.authentication import TokenAuthentication
from rest_framework.settings import api_settings
from django.db.models import Model as DjangoModel
from auth_submodule.redis_proxy import RedisInvertibleDict


class UserTokenRID(RedisInvertibleDict):
    redis_connection_name = 'token'
    forward_key_prefix = 'token'
    reverse_key_prefix = 'user'
    keys_ttl = 60


def get_user_by_id(user_id: int) -> DjangoModel:
    user_model = get_user_model()
    try:
        user = user_model.objects.get(pk=user_id)
    except user_model.DoesNotExist:
        return api_settings.UNAUTHENTICATED_USER()
    return user


def get_token_by_key(model: DjangoModel, token: str):
    try:
        return model.objects.get(key=token)
    except model.DoesNotExist:
        return


class RedisTokenAuthentication(TokenAuthentication):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.token_cache = UserTokenRID()

    def authenticate_credentials(self, key: str):
        model = self.get_model()
        user_id = self.token_cache[key]
        if user_id:
            return (SimpleLazyObject(lambda: get_user_by_id(user_id)),
                    SimpleLazyObject(lambda: get_token_by_key(model, key)))
        user_obj, token_obj = super().authenticate_credentials(key)
        self.token_cache.inv[user_obj.id] = token_obj.key
        return user_obj, token_obj

