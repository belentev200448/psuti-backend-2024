from advantage.drf.queryset.worker_base import WorkerBase
from advantage.drf.queryset.serializers import WorkerMinMaxSerializer, WorkerBoolSerializer
from abc import abstractmethod


class FilterWorkerBase(WorkerBase):

    @abstractmethod
    def apply(self, qs):
        return qs


class FilterWorkerMinMax(FilterWorkerBase):
    type_name = "min_max"
    serializer = WorkerMinMaxSerializer

    def apply(self, qs):
        value_min = self.data.get('value_min')
        value_max = self.data.get('value_max')
        filter_kwargs = {}
        if value_min:
            filter_kwargs.update({self.field + '__gte': value_min})
        if value_max:
            filter_kwargs.update({self.field + '__lte': value_max})
        return qs.filter(**filter_kwargs)


class FilterWorkerBool(FilterWorkerBase):
    type_name = 'true_false_both'
    serializer = WorkerBoolSerializer

    def apply(self, qs):
        value_ = self.data.get('value')
        if isinstance(value_, bool):
            filter_kwargs = {self.field_name: value_}
            return qs.filter(**filter_kwargs)
        return qs  # TODO mb return serialization error


class FilterWorkerDatetimeMinMax(FilterWorkerMinMax):
    type_name = 'datetime_min_max'

    def normalize_data(self, data):
        self.data = data
