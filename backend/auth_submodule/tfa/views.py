from advantage.drf.viewsets import SmartGenericViewSet


class TFAViewSetGeneric(SmartGenericViewSet):
    auth_controller = None
    auth_identifier_field = None

    def get_auth_controller(self):
        assert self.auth_controller is not None, (
            f"`auth_controller` attribute in {self.__class__.__name__}"
            f" must not be None or override `get_auth_controller` method")
        return self.auth_controller

    def get_auth_identifier_field(self):
        assert self.auth_identifier_field is not None, (
            f"`auth_identifier_field` attribute in {self.__class__.__name__}"
            f" must not be None or override `get_auth_identifier_field` method")
        return self.auth_identifier_field

    def send_code(self, request, auth_identifier):
        tfa = self.get_auth_controller()(auth_identifier)
        return tfa.send_auth_code()

    def validate(self, request, auth_identifier, code):
        tfa = self.get_auth_controller()(auth_identifier)
        tfa.is_code_valid(code)
        return auth_identifier
