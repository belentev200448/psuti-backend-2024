from rest_framework import serializers
import os
from advantage.recaptcha.main import RecaptchaController, RecaptchaVirtualSpace


class RecaptchaWebController(RecaptchaController):
    site_key = os.environ.get('recaptcha_web_key')
    project_id = os.environ.get('gcloud_project_id')


class RecaptchaVirtualSpaceGeneric(RecaptchaVirtualSpace):
    platforms = {
        'web': RecaptchaWebController,
    }
    min_success_score = 0.1


class RecaptchaParamsSerializer(serializers.Serializer):
    platform = serializers.CharField()
    token = serializers.CharField()
