from random import randint
from rest_framework.exceptions import ValidationError
from .store import TFACodeRedisStoreGeneric
from .sender import CodeSenderBase


class TFAAuthControllerGeneric:
    store_cls = None
    sender_cls = None
    code_length = None
    code_lifetime = None

    def __init__(self, phone_number: str):
        self.phone_number = phone_number
        self.store = self.get_store_cls()(phone_number)
        self.sender = self.get_sender_cls()()

    @classmethod
    def get_code_length(cls) -> int:
        assert cls.code_length is not None, (f"`code_length` attribute in {cls.__class__.__name__}"
                                              f" must not be None or override `get_code_length` method")
        assert cls.code_length is not int, (f"`code_length` attribute in {cls.__class__.__name__}"
                                              f" must be a valid integer number")
        assert cls.code_length > 0, (f"`code_length` attribute in {cls.__class__.__name__}"
                                              f" must be rather that 0")
        return cls.code_length

    def get_code_lifetime(self) -> int:
        assert self.code_lifetime is not None, (f"`code_lifetime` attribute in {self.__class__.__name__}"
                                              f" must not be None or override `get_code_lifetime` method")
        assert self.code_lifetime is not int, (f"`code_lifetime` attribute in {self.__class__.__name__}"
                                              f" must be a valid integer number")
        assert self.code_lifetime > 0, (f"`code_lifetime` attribute in {self.__class__.__name__}"
                                     f" must be rather that 0")
        return self.code_lifetime

    def get_store_cls(self) -> type[TFACodeRedisStoreGeneric]:
        assert self.store_cls is not None, (f"`store_cls` attribute in {self.__class__.__name__}"
                                              f" must not be None or override `get_store_cls` method")
        return self.store_cls

    def get_sender_cls(self) -> type[CodeSenderBase]:
        assert self.sender_cls is not None, (f"`sender_cls` attribute in {self.__class__.__name__}"
                                              f" must not be None or override `get_sender_cls` method")
        return self.sender_cls

    @classmethod
    def get_code_min(cls):
        cl = cls.get_code_length()
        return int('1' * cl)

    @classmethod
    def get_code_max(cls):
        cl = cls.get_code_length()
        return int('9' * cl)

    def generate_auth_code(self):
        c_min = self.get_code_min()
        c_max = self.get_code_max()
        code = randint(c_min, c_max)
        return code

    def send_auth_code(self):
        if self.store.exists:
            raise ValidationError('Auth code already sent')
        code = self.generate_auth_code()
        self.store.add_code(code, self.get_code_lifetime())
        self.sender.send_code(self.phone_number, code)
        return code

    def is_code_valid(self, code: int):
        user_code = code
        sent_code = self.store.code
        if not sent_code:
            raise ValidationError('Auth code not exists')
        if user_code != sent_code:
            raise ValidationError('Invalid code')
        return self.store.delete_code()
