from advantage.drf.queryset.serializers import WorkerBoolPoseSerializer
from advantage.drf.queryset.worker_base import WorkerBase


class SorterWorkerBase(WorkerBase):
    pass


class SorterWorkerAscDesc(SorterWorkerBase):
    type_name = 'asc_desc'
    serializer = WorkerBoolPoseSerializer


class SorterWorkerTrueFalse(SorterWorkerAscDesc):
    type_name = 'true_false'
