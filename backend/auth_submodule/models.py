from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from auth_submodule.managers import CustomUserManager


class User(AbstractUser):
    username = None
    middle_name = models.CharField(
        _("Отчество"),
        max_length=30,
        blank=True,
        null=True
    )
    email = models.EmailField(
        _("email address"),
        unique=True
    )
    is_verified = models.BooleanField(
        _("user is verified"),
        default=False,
    )

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        if not self.last_name or not self.first_name:
            return f'{self.email}'
        if not self.middle_name:
            return f'{self.last_name} {self.first_name}'
        return f'{self.last_name} {self.first_name} {self.middle_name}'

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


