# psuti-backend-2024

## Запуск проекта
- [ ] создать docker volume для psql:
```shell
docker volume create psuti-psql
```
- [ ] запустить
```shell
docker-compose --env-file .env.dev up --build -d
```

## Swagger (документация)
```dev/swagger/```

## Recaptcha
для выполнения запросов нужно ввести код рекапчи
для получения кода необходимо перейти на
```/auth/recaptcha/```
ввести action, указанный в описании каждого эндпоинта в swagger
получить код, полученный код можно использовать в защищённых эндпоинтах
- platform: `web`

## Авторизация
in header `Authorization`: `Token 52112dd04d160e047421330e96c87da039532f88`

## отправка кодов на email
В проекте есть celery, для фоновой отправки кодов на email, однако логика не реализована, так как под рукой не оказалось
dev ключей.
