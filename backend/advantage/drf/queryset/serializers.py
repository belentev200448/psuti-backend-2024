from rest_framework import serializers


class BlankSerializer(serializers.Serializer):
    pass


class WorkerMinMaxSerializer(serializers.Serializer):
    value_min = serializers.IntegerField(allow_null=True, required=False)
    value_max = serializers.IntegerField(allow_null=True, required=False)

    def validate(self, data):
        value_min = data.get('value_min')
        value_max = data.get('value_max')

        if not value_min and not value_max:
            raise serializers.ValidationError(
                {'value error': ['To `min_max` filter you must provide `value_min` or `value_max` or both']}
            )
        return data


class WorkerBoolSerializer(serializers.Serializer):
    value = serializers.BooleanField(allow_null=True)


class WorkerBoolPoseSerializer(serializers.Serializer):
    value = serializers.BooleanField(allow_null=True)
    pose = serializers.IntegerField()


class PageNumberPaginationSerializer(serializers.Serializer):
    page = serializers.IntegerField(required=False, default=1)
    per_page = serializers.IntegerField(required=False, default=10)
