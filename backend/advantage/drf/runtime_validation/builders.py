from types import FunctionType
from rest_framework.response import Response


def class_view_builder(
        fabric_cls,
        f: FunctionType,
        validated_request_func: FunctionType,
        validated_response_func: FunctionType,
        response_status: FunctionType,
        ):
    def decorator(self, request, *args, **kwargs):
        validated_request_data = validated_request_func(request, *args, **kwargs)
        result = f(self, request, *args, **kwargs, **validated_request_data)
        if isinstance(result, tuple):
            validated_response_data = validated_response_func(*result)
        else:
            validated_response_data = validated_response_func(result, {})
        return Response(validated_response_data, status=response_status())
    return decorator


def action_view_builder(
        fabric_cls,
        f: FunctionType,
        validated_request_func: FunctionType,
        validated_response_func: FunctionType,
        response_status: FunctionType,
        ):
    def decorator(self, request, *args, **kwargs):
        validated_request_data = validated_request_func(request, *args, **kwargs)
        result = f(self, request, *args, **kwargs, **validated_request_data)
        if isinstance(result, tuple):
            validated_response_data = validated_response_func(*result)
        else:
            validated_response_data = validated_response_func(result, {})
        return Response(validated_response_data, status=response_status())
    return decorator