from typing import TypeVarTuple, Generic, Type, TypeVar
from abc import ABC, abstractmethod


SerializerTypeList = TypeVarTuple('SerializerTypeList')
SerializerType = TypeVar('SerializerType')
BaseDataType = TypeVar('BaseDataType')


class SerializerChoicerBase(Generic[*SerializerTypeList], ABC):
    def __init__(self, *serializers_list: *Type[SerializerTypeList]):
        self.serializers_list = serializers_list

    @abstractmethod
    def get_base_data(self, external_data):
        pass

    @abstractmethod
    def get_serializer_class(self, base_data):
        pass

    @abstractmethod
    def get_serializer_kwargs(self, base_data) -> dict:  # usually is many kwargs
        pass

    def __call__(self, external_data, **kwargs):  # -> Type[*SerializerTypeList]:
        base_data = self.get_base_data(external_data)
        sr = self.get_serializer_class(base_data)
        sr_kwargs = self.get_serializer_kwargs(base_data)
        return sr(**kwargs, **sr_kwargs)


class SerializerChoicerSimple(SerializerChoicerBase):
    def get_base_data(self, external_data):
        return

    def get_serializer_class(self, base_data):
        return self.serializers_list[0]

    def get_serializer_kwargs(self, base_data):
        return {}


class SerializerChoicerSimpleMany(SerializerChoicerSimple):
    def get_serializer_kwargs(self, base_data):
        return {"many": True}

