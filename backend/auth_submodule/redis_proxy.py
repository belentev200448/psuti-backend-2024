from __future__ import annotations
from collections.abc import MutableMapping
from typing import TypeVar
from django.core.cache import caches


KT = TypeVar('KT')
VT = TypeVar('VT')


class RedisInvertibleDict(MutableMapping[VT, KT]):
    redis_connection_name: str
    forward_key_prefix: str
    reverse_key_prefix: str
    keys_ttl: int

    def _get_forward_key_prefix(self):
        assert self.forward_key_prefix is not None
        assert isinstance(self.forward_key_prefix, str)
        return self.forward_key_prefix

    def _get_reverse_key_prefix(self):
        assert self.reverse_key_prefix is not None
        assert isinstance(self.reverse_key_prefix, str)
        return self.reverse_key_prefix

    def _get_redis_connection_name(self):
        assert self.redis_connection_name is not None
        assert isinstance(self.redis_connection_name, str)
        return self.redis_connection_name

    def _get_keys_ttl(self):
        return self.keys_ttl

    def _get_redis_connection(self):
        return caches[self._get_redis_connection_name()]

    def __init__(self, /, *, _reverse=False):
        if not _reverse:
            self._forward = self._get_forward_key_prefix()
            self._reverse = self._get_reverse_key_prefix()
        else:
            self._forward = self._get_reverse_key_prefix()
            self._reverse = self._get_forward_key_prefix()

    @staticmethod
    def _get_redis_key(key: KT, prefix: str):
        return f"{prefix}__{key}"

    def __getitem__(self, item: KT) -> VT:
        cache = self._get_redis_connection()
        key = self._get_redis_key(item, self._forward)
        res = cache.get(key)
        if not res:
            return res
        key_back = self._get_redis_key(res, self._reverse)
        cache.expire(key, timeout=self._get_keys_ttl())
        cache.expire(key_back, timeout=self._get_keys_ttl())
        return res

    def __setitem__(self, key: KT, value: VT):
        cache = self._get_redis_connection()
        missing = object()

        old_key = cache.get(self._get_redis_key(value, self._reverse)) or missing
        if old_key is not missing and old_key != key:  # {1: "a"} -> {1: "a", 2: "a"}
            # value is already mapped to a different key
            self._raise_non_invertible(old_key, key, value)

        old_value = cache.get(self._get_redis_key(key, self._forward)) or missing
        if old_value is not missing:
            cache.delete(self._get_redis_key(old_value, self._reverse))

        cache.set(self._get_redis_key(key, self._forward), value, self._get_keys_ttl())
        cache.set(self._get_redis_key(value, self._reverse), key, self._get_keys_ttl())

    def __delitem__(self, key: KT):
        cache = self._get_redis_connection()
        value = cache.get(self._get_redis_key(key, self._forward))
        cache.delete(self._get_redis_key(key, self._forward))
        cache.delete(self._get_redis_key(value, self._reverse))

    def __iter__(self):
        raise ValueError('You cannot iterate redis proxy object')

    def __len__(self):
        raise ValueError('You cannot get length of redis proxy object')

    @property
    def inv(self) -> RedisInvertibleDict[VT, KT]:
        """A mutable view of the inverse dict."""
        return self.__class__(_reverse=True)

    def _raise_non_invertible(self, key1: KT, key2: KT, value: VT):
        raise ValueError(f"non-invertible: {key1}, {key2} both map to: {value}")


