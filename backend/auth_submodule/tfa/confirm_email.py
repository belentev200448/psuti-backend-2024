from django.contrib.auth import get_user_model
from advantage.drf.utils import get_object_or_none
from auth_submodule.tfa_auth_base import TFAAuthControllerGeneric, CodeSenderBase, TFACodeRedisStoreGeneric
from requests_toolbelt.multipart.encoder import MultipartEncoder
import httpx
import os


class EMAILCodeSenderInfobip(CodeSenderBase):
    template = None

    def get_request_headers(self, encoder):
        return {

        }

    @staticmethod
    def get_message_text(code: int):
        return f'Здравствуйте!\nВаш код подтверждения email: {code}',

    def get_request_content(self, auth_identifier, code):
        return

    def send_code(self, auth_identifier: str, code: int):
        return  # TODO Enter you're send code logic


class EMAILCodeStore(TFACodeRedisStoreGeneric):
    redis_connection_name = 'confirm_email'


class ConfirmEmailController(TFAAuthControllerGeneric):
    store_cls = EMAILCodeStore
    sender_cls = EMAILCodeSenderInfobip
    code_length = 6
    code_lifetime = 300
