from functools import wraps
from rest_framework.decorators import action
from advantage.drf.func_validators.processors import FunctionValidatorProcessor
from advantage.drf.runtime_validation.base import SchemeGenerator, \
    SmartActionValidationFabricRuntime


def smart_action(
    methods=None,
    detail=None,
    url_path=None,
    url_name=None,
    **kwargs
):
    def decorator(f):
        function_validator_obj = FunctionValidatorProcessor()
        function_validator_obj.validate(f)
        decorator_fabric = SmartActionValidationFabricRuntime(f, methods)
        decorated = decorator_fabric.decorate()
        wrap_decorator = wraps(f)
        decorated = wrap_decorator(decorated)
        action_decorator = action(methods=methods, detail=detail, url_path=url_path, url_name=url_name, **kwargs)
        action_decorated = action_decorator(decorated)
        schema_fabric = SchemeGenerator(action_decorated, decorator_fabric.context)
        extended = schema_fabric.decorate()
        return extended
    return decorator

