from django.core.cache import caches


class TFACodeRedisStoreGeneric:
    redis_connection_name = None

    def get_redis_connection_name(self):
        assert self.redis_connection_name is not None, (f"`redis_connection_name` attribute in {self.__class__.__name__}"
                                              f" must not be None or override `get_redis_connection_name` method")
        return self.redis_connection_name

    def get_redis_client(self):
        conn = self.get_redis_connection_name()
        return caches[conn]

    @property
    def client(self):
        return self.get_redis_client()

    def __init__(self, auth_identifier: str):
        self.auth_identifier = auth_identifier

    @property
    def code(self):
        return self.client.get(self.auth_identifier)

    @property
    def exists(self):
        return bool(self.code)

    def add_code(self, code: int, lifetime: int):
        client = self.client
        return client.set(self.auth_identifier, code, timeout=lifetime)

    def delete_code(self):
        return self.client.delete(self.auth_identifier)
