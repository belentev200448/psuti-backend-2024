from django.urls import path, include
from rest_framework.routers import DefaultRouter
from auth_submodule.views import *


app_name = 'auth_submodule'


email = DefaultRouter()
email.register(
    'auth',
    EmailAuthViewSet,
    basename='auth'
)
email.register(
    'confirm',
    EmailConfirmViewSet,
    basename='confirm'
)
email.register(
    'reset_password',
    PasswordResetViewSet,
    basename='reset_password'
)

user_data = DefaultRouter()
user_data.register(
    'data',
    UserData,
    'data'
)

urlpatterns = [
    path('email/', include(email.urls), name='email'),
    path('user/', include(user_data.urls), name='user'),
    path('recaptcha/', recaptcha, name='recaptcha'),
]

