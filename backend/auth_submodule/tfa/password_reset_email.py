from auth_submodule.tfa_auth_base import TFAAuthControllerGeneric, CodeSenderBase, TFACodeRedisStoreGeneric
from requests_toolbelt.multipart.encoder import MultipartEncoder
import httpx
import os


class EMAILCodeSenderInfobip(CodeSenderBase):
    template = None

    def get_request_headers(self, encoder):
        return {
        }

    @staticmethod
    def get_message_text(code: int):
        return f'Здравствуйте!\nВаш код подтверждения для сброса пароля: {code}'

    def get_request_content(self, auth_identifier, code):
        return

    def send_code(self, auth_identifier: str, code: int):
        return  # TODO Enter you're send code logic


class EMAILCodeStore(TFACodeRedisStoreGeneric):
    redis_connection_name = 'reset_password'


class PasswordResetController(TFAAuthControllerGeneric):
    store_cls = EMAILCodeStore
    sender_cls = EMAILCodeSenderInfobip
    code_length = 6
    code_lifetime = 300

