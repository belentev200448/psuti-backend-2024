from abc import ABC, abstractmethod


class CodeSenderBase(ABC):
    template = None

    def get_template(self):
        assert self.template is not None, (
            f"`template` attribute in {self.__class__.__name__}"
            f" must not be None or override `get_template` method")
        return self.template

    def render_template(self, **kwargs):
        return self.get_template().format(**kwargs)

    @abstractmethod
    def send_code(self, auth_identifier: str, code: int):
        pass
