import inspect
from rest_framework.request import Request
from advantage.drf.func_validators.base import FuncValidatorBase, SingHasArgGeneric, AnnotationsTypeGeneric, \
    CheckSignAntMatchGeneric, AntTypeNameHardCheck
from advantage.drf.func_validators.exceptions import SmartApiViewInitValidationException
from advantage.drf.func_validators.utils import typehint_is_serializer_or_choicer


class SingHasRequestArg(SingHasArgGeneric):
    """
    Проверка наличия request аргумента в сигнатуре view функции
    """
    arg = 'request'


class SingHasReturnAnt(FuncValidatorBase):
    """
    Проверка наличия аннотации возвращаемого типа в сигнатуре view функции
    """
    def get_exception(self):
        return SmartApiViewInitValidationException(
            error_detail=f'return annotation must not be empty in `{self.method_name}`',
            cls_path=self.cls_path,
        )

    def check(self):
        if self.signature.return_annotation is inspect._empty:
            raise self.get_exception()


class DisallowedSign(FuncValidatorBase):
    disallowed_signatures = {
        'get': ('body',),
        'delete': ('body',),
    }
    """
    Проверка наличия аннотации запрещённого аргумента для view функции определённого метода
    """
    def get_exception(self):
        sings = self.disallowed_signatures.get(self.method_name)
        return SmartApiViewInitValidationException(
            error_detail=f'`{self.method_name}` method cannot have `({" ".join(sings)})` arguments',
            cls_path=self.cls_path,
        )

    def check(self):
        sings = self.disallowed_signatures.get(self.method_name)
        if not sings:
            return
        for sing in sings:
            if self.annotations.get(sing):
                return self.get_exception()



class RequestArgAreDRF(AnnotationsTypeGeneric):
    arg = 'request'
    type = Request


class AllArgsHasAnt(CheckSignAntMatchGeneric):
    exclude_args = ('self',)


class SerializersArgsAntName(AntTypeNameHardCheck):
    types = {
        typehint_is_serializer_or_choicer: ('body', 'params', 'return'),
    }
