

class StaticValidationBaseException(Exception):
    template = """
    ADVANTAGE ERROR
    At `{technology_name}` in `{cls_path}`:
    {error_detail}
    """
    technology_name = None

    def get_error_detail(self):
        return self.template.format(
            technology_name=self.technology_name,
            cls_path=self.cls_path,
            error_detail=self.error_detail,
        )

    def __init__(self, error_detail, cls_path):
        self.error_detail = error_detail
        self.cls_path = cls_path
        super().__init__(self.get_error_detail())


class SmartApiViewInitValidationException(StaticValidationBaseException):
    technology_name = "SmartApiView"
