import inspect
from typing import get_origin, get_args

from django.http import QueryDict
from rest_framework.serializers import BaseSerializer
from advantage.drf.serializer_choicer import SerializerChoicerBase


def is_serializer(cls): return inspect.isclass(cls) and issubclass(cls, BaseSerializer)


def is_serializer_choicer(cls): return inspect.isclass(cls) and issubclass(cls, SerializerChoicerBase)


def is_serializer_or_choicer(cls): return is_serializer(cls) or is_serializer_choicer(cls)


def typehint_is_serializer_or_choicer(type_hint):
    if is_serializer(type_hint):
        return True
    origin = get_origin(type_hint)
    sr_list = get_args(type_hint)
    if origin is list:
        if len(sr_list) != 1:
            return False
        elif is_serializer(sr_list[0]):
            return True
    elif inspect.isclass(origin) and issubclass(origin, SerializerChoicerBase):
        return True


def is_iterable(obj): return hasattr(obj, '__iter__') or (hasattr(obj, '__getitem__') and callable(getattr(obj, '__getitem__')))


def query_to_dict(query: QueryDict): return {
            key: query.getlist(key)
            if len(query.getlist(key)) > 1
            else query.get(key)
            for key in query
        }


def get_cls_qualname_meta(name: str, dict_qualname: str):
    if dict_qualname:
        dict_qualname = dict_qualname.rpartition('.')[0]
        if dict_qualname:
            return f"{dict_qualname}.{name}"
        else:
            return name
    else:
        return name
