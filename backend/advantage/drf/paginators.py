from django.core.paginator import Paginator
from advantage.drf.queryset.serializers import PageNumberPaginationSerializer


class PageNumberPagination:
    display_page_controls = False
    # page_field = "page" # TODO DRF dynamic field name changing
    # per_page_field = "per_page" # TODO DRF dynamic field name changing

    def get_schema_operation_parameters(self, view):
        # Your logic for schema operation parameters
        return []

    def get_paginated_response_schema(self, schema):
        return {
            'type': 'object',
            'properties': {
                'count': {
                    'type': 'integer',
                    'example': 123,
                },
                'max_pages': {
                    'type': 'integer',
                    'example': 123,
                },
                'results': schema,
            },
        }

    @staticmethod
    def get_base_data(request):
        sr = PageNumberPaginationSerializer(data=request.data)
        sr.is_valid(raise_exception=True)
        return sr.validated_data

    def get_queryset_count(self, queryset):   # TODO make it optional
        return queryset.count()

    def paginate_queryset(self, queryset, request, view):
        request_data = self.get_base_data(request)
        page = request_data.get('page')
        per_page = request_data.get('per_page')
        queryset_count = self.get_queryset_count(queryset)
        paginator = Paginator(queryset, per_page)
        num_pages = paginator.num_pages
        if num_pages < page:
            page = paginator.num_pages
        page_obj = paginator.get_page(page)
        return page_obj, queryset_count, num_pages

