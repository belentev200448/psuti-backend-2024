from rest_framework.authtoken.models import Token
from auth_submodule.authentication import UserTokenRID


def get_user_token(user):
    rid = UserTokenRID()
    token = rid.inv[user.id]
    if token:
        return {'authtoken': token}
    token, _ = Token.objects.get_or_create(user=user)
    rid[token.key] = user.id
    return {'authtoken': token.key}
