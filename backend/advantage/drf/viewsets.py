from rest_framework import mixins

from advantage.drf.mixins import SmartListMixin, SmartListModelMixin
from advantage.drf.views import SmartGenericAPIView
from rest_framework.viewsets import ViewSetMixin


class SmartGenericViewSet(ViewSetMixin, SmartGenericAPIView):
    pass


class SmartViewSet(SmartListMixin, SmartGenericViewSet):
    pass


class SmartModelViewSet(mixins.CreateModelMixin,
                        mixins.RetrieveModelMixin,
                        mixins.UpdateModelMixin,
                        mixins.DestroyModelMixin,
                        SmartListModelMixin,
                        SmartListMixin,
                        SmartGenericViewSet):
    pass
