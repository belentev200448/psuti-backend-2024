import inspect
from advantage.drf.func_validators.utils import is_iterable
from advantage.drf.func_validators.validators import SingHasRequestArg, SingHasReturnAnt, RequestArgAreDRF, \
    AllArgsHasAnt, SerializersArgsAntName, DisallowedSign
from types import FunctionType
from typing import get_type_hints


class FunctionValidatorProcessorBase:
    validators = None

    def __init__(self, cls_path=None):
        self.cls_path = cls_path

    @staticmethod
    def get_base_data(f: FunctionType) -> tuple:
        signature = inspect.signature(f)
        annotations = get_type_hints(f)
        return signature, annotations

    def get_validators(self):
        assert self.validators is not None,\
            f'{self.__class__.__qualname__} must have "validators" class attr or override get_validators method'
        assert is_iterable(self.validators), \
            f'Class attr "validators" in {self.__class__.__qualname__} must be iterable'
        return self.validators

    def validate(self, f: FunctionType):
        base_data = self.get_base_data(f)
        for validator in self.validators:
            validator(f.__name__, *base_data, cls_path=self.cls_path).check()

    def validate_list(self, f_list):
        for f in f_list:
            self.validate(f)


class FunctionValidatorProcessor(FunctionValidatorProcessorBase):
    validators = [
        SingHasRequestArg,
        AllArgsHasAnt,
        RequestArgAreDRF,
        SingHasReturnAnt,
        SerializersArgsAntName,
        DisallowedSign,
    ]


