from rest_framework import serializers
from django.contrib.auth.password_validation import validate_password
from auth_submodule.models import User
from auth_submodule.tfa import PasswordResetController
from auth_submodule.tfa.confirm_email import ConfirmEmailController


class TokenAuthInputSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(max_length=128)


class TokenAuthOutputSerializer(serializers.Serializer):
    authtoken = serializers.CharField()


class UserRegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    confirm_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'middle_name', 'password', 'confirm_password')

    def validate(self, data):
        if data['password'] != data['confirm_password']:
            raise serializers.ValidationError({"confirm_password": "Password fields didn't match."})

        return data

    def create(self, validated_data):
        validated_data.pop('confirm_password')
        user = User.objects.create_user(
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            middle_name=validated_data['middle_name'],
            password=validated_data['password']
        )
        return user


class EmailConfirmCodeSerializer(serializers.Serializer):
    code = serializers.IntegerField(
        min_value=ConfirmEmailController.get_code_min(),
        max_value=ConfirmEmailController.get_code_max(),
    )


class PasswordResetEmailSerializer(serializers.Serializer):
    email = serializers.EmailField()


class PasswordResetCodeSerializer(serializers.Serializer):
    code = serializers.IntegerField(
        min_value=PasswordResetController.get_code_min(),
        max_value=PasswordResetController.get_code_max(),
    )
    email = serializers.EmailField()
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    confirm_password = serializers.CharField(write_only=True, required=True)

    def validate(self, data):
        if data['password'] != data['confirm_password']:
            raise serializers.ValidationError({"confirm_password": "Password fields didn't match."})

        return data


class UserDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'middle_name', 'is_verified')
        extra_kwargs = {
            'is_verified': {'read_only': True},
            'email': {'read_only': True},
        }
