from typing import Dict, Tuple, Type
from types import FunctionType
from advantage.drf.func_validators.processors import FunctionValidatorProcessor
from advantage.drf.func_validators.utils import get_cls_qualname_meta
from advantage.drf.runtime_validation.base import SmartAPIViewValidationFabricRuntime, SchemeGenerator

functions_dict_type = Dict[str, FunctionType]


class SmartApiViewMeta(type):

    def __new__(cls, name: str, bases: Tuple[Type, ...], attrs: functions_dict_type):
        super_new = super().__new__

        parents = [b for b in bases if isinstance(b, SmartApiViewMeta)]
        if not parents:
            return super_new(cls, name, bases, attrs)

        http_method_names = bases[0].http_method_names
        methods_available = [attrs.get(m) for m in http_method_names if attrs.get(m)]

        cls_path = f"{attrs.get('__module__')}.{get_cls_qualname_meta(name, attrs.get('__qualname__'))}"
        function_validator_obj = FunctionValidatorProcessor(cls_path=cls_path)
        function_validator_obj.validate_list(methods_available)

        for method_func in methods_available:
            decorator_fabric = SmartAPIViewValidationFabricRuntime(method_func)
            decorated = decorator_fabric.decorate()
            schema_fabric = SchemeGenerator(decorated, decorator_fabric.context)
            extended = schema_fabric.decorate()
            attrs.update({method_func.__name__: extended})

        new_class = super_new(cls, name, bases, attrs)
        return new_class
