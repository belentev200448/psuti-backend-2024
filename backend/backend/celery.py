import os
from time import sleep
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tgbot.settings')
app = Celery("tgbot")
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.task
def sleep_(x):
    sleep(x)


app.conf.beat_schedule = {

}
