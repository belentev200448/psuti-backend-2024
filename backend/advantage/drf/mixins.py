from rest_framework.response import Response
from rest_framework.decorators import action


class SmartListMixin:

    @action(detail=False, methods=['post'])
    def smart_list(self, request, *args, **kwargs):
        queryset = self.process_queryset(self.get_queryset())
        queryset, queryset_count, num_pages = self.paginate_queryset(queryset)
        serializer = self.get_serializer(queryset, many=True)
        response = {
            'count': queryset_count,
            'max_pages': num_pages,
            'results': serializer.data,
        }
        return Response(response)


class SmartListModelMixin:
    """
    List a queryset.
    """
    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

