from types import FunctionType
from typing import get_type_hints, get_origin, get_args
from rest_framework.request import Request
from advantage.drf.func_validators.utils import query_to_dict, is_serializer
from advantage.drf.serializer_choicer import SerializerChoicerSimple, SerializerChoicerSimpleMany, SerializerChoicerBase
from advantage.drf.runtime_validation.builders import class_view_builder
from abc import ABC, abstractmethod
from drf_spectacular.utils import extend_schema, PolymorphicProxySerializer, OpenApiParameter
from uuid import uuid4


class SchemeGenerator:

    def __init__(self, f: FunctionType, context: dict):
        self.f = f
        self.context = context

    def get_serializer_class_by_context(self, arg_name):
        serializer = self.context.get(arg_name)
        if isinstance(serializer, SerializerChoicerSimpleMany):
            return serializer.serializers_list[0](many=True)
        elif isinstance(serializer, SerializerChoicerSimple):
            return serializer.serializers_list[0]()
        elif isinstance(serializer, SerializerChoicerBase):
            return PolymorphicProxySerializer(
                component_name=str(uuid4()),  # FIXME random name is bad practice
                serializers=serializer.serializers_list,
                resource_type_field_name=None,
                many=False,
            )

    def get_decorator(self):
        kwargs = {
            'parameters': [self.get_serializer_class_by_context('params')],
            'request': self.get_serializer_class_by_context('body'),
            'responses': self.get_serializer_class_by_context('response'),
        }
        scheme_decorator = extend_schema(
            **kwargs
        )
        return scheme_decorator

    def decorate(self):
        decorator = self.get_decorator()
        return decorator(self.f)


class RuntimeValidationDecoratorFabricBase(ABC):
    builder = None
    scheme_generator = None

    @abstractmethod
    def make_validate_request_func(self) -> FunctionType:
        pass

    @abstractmethod
    def make_validate_response_func(self) -> FunctionType:
        pass

    @abstractmethod
    def make_response_status_code_func(self) -> FunctionType:
        pass

    @abstractmethod
    def decorate(self) -> FunctionType:
        pass


class SmartAPIViewValidationFabricRuntime(RuntimeValidationDecoratorFabricBase):
    builder = class_view_builder
    scheme_generator = SchemeGenerator

    def get_scheme_generator(self):  # TODO refactor
        pass

    def __init__(self, f: FunctionType):
        self.f = f
        self.annotations = get_type_hints(f)
        self.context = {}
        self.validate_request_func = None
        self.validate_response_func = None
        self.response_status_code_func = None

    def make_validate_request_func(self):
        params = self.get_serializer_choicer('params')
        body = self.get_serializer_choicer('body')

        if body and params:
            def validator(request: Request):
                params_sr = params(external_data=request,
                                   data=query_to_dict(request.query_params),
                                   context={'request': request})
                params_sr.is_valid(raise_exception=True)
                body_sr = body(external_data=request, data=request.data, context={'request': request})
                body_sr.is_valid(raise_exception=True)
                return {'params': params_sr, 'body': body_sr}
        elif body and not params:
            def validator(request: Request):
                body_sr = body(external_data=request, data=request.data, context={'request': request})
                body_sr.is_valid(raise_exception=True)
                return {'body': body_sr}
        elif not body and params:
            def validator(request: Request):
                params_sr = params(external_data=request,
                                   data=query_to_dict(request.query_params),
                                   context={'request': request})
                params_sr.is_valid(raise_exception=True)
                return {'params': params_sr}
        else:
            def validator(request: Request): return {}
        self.context.update(body=body, params=params)
        self.validate_request_func = validator

    def make_validate_response_func(self):
        return_sr = self.get_serializer_choicer('return')

        def validator(response_data, sr_kwargs):
            sr = return_sr(external_data=(response_data, sr_kwargs), instance=response_data, **sr_kwargs)
            return sr.data

        self.context.update(response=return_sr)
        self.validate_response_func = validator

    def make_response_status_code_func(self):
        self.response_status_code_func = lambda: 200

    def get_serializer_choicer(self, arg: str):
        type_hint = self.annotations.get(arg)
        if not type_hint:
            return

        if is_serializer(type_hint):
            return SerializerChoicerSimple(type_hint)

        origin = get_origin(type_hint)
        sr_list = get_args(type_hint)
        if origin is list:
            return SerializerChoicerSimpleMany(*sr_list)
        elif issubclass(origin, SerializerChoicerBase):
            return origin(*sr_list)

        raise Exception('API view compile error, serializer annotation type not supported')

    def decorate(self):
        self.make_validate_request_func()
        self.make_validate_response_func()
        self.make_response_status_code_func()
        decorated = self.builder(
            self.f,
            self.validate_request_func,
            self.validate_response_func,
            self.response_status_code_func,
        )
        return decorated


class SmartActionValidationFabricRuntime(SmartAPIViewValidationFabricRuntime):
    body_available_http_methods = ('POST', 'PUT', 'PATCH')

    def __init__(self, f, methods):
        super().__init__(f)
        self.methods = methods

    def make_validate_request_func(self):
        params = self.get_serializer_choicer('params')
        body = self.get_serializer_choicer('body')

        if body and params:
            def validator(request: Request, *args, **kwargs):
                external_data = {'request': request, 'args': args, 'kwargs': kwargs}
                params_sr = params(external_data=external_data, data=query_to_dict(request.query_params))
                params_sr.is_valid(raise_exception=True)
                out = {'params': params_sr, 'body': None}
                if request.method in self.body_available_http_methods:
                    body_sr = body(external_data=external_data, data=request.data)
                    body_sr.is_valid(raise_exception=True)
                    out.update(body=body_sr)
                return out
        elif body and not params:
            def validator(request: Request, *args, **kwargs):
                out = {'body': None}
                if request.method in self.body_available_http_methods:
                    external_data = {'request': request, 'args': args, 'kwargs': kwargs}
                    body_sr = body(external_data=external_data, data=request.data)
                    body_sr.is_valid(raise_exception=True)
                    out.update(body=body_sr)
                return out
        elif not body and params:
            def validator(request: Request, *args, **kwargs):
                external_data = {'request': request, 'args': args, 'kwargs': kwargs}
                params_sr = params(external_data=external_data, data=query_to_dict(request.query_params))
                params_sr.is_valid(raise_exception=True)
                return {'params': params_sr}
        else:
            def validator(request: Request, *args, **kwargs): return {}
        self.context.update(body=body, params=params)
        self.validate_request_func = validator
