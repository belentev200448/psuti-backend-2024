from abc import ABC, abstractmethod
from operator import attrgetter

from django.db.models import Q

from advantage.drf.queryset.filters import FilterWorkerMinMax, FilterWorkerBool, FilterWorkerDatetimeMinMax
from advantage.drf.queryset.sorters import SorterWorkerAscDesc, SorterWorkerTrueFalse


class QuerysetProcessorBase(ABC):
    field = None

    def get_base_data(self, request):
        return request.data.get(self.field)

    @abstractmethod
    def prepare(self, request):
        pass

    @abstractmethod
    def apply(self, qs):
        return qs

    def process_queryset(self, request, queryset, apiview_obj):
        self.prepare(request)
        return self.apply(queryset)


class QuerysetProcessorWorkersBase(QuerysetProcessorBase):
    workers = None

    def __init__(self):
        self.workers_objs = []
        self.workers_type_dict = {worker.type_name: worker for worker in self.workers}

    def create_worker_by_type(self, worker_field, worker_data):
        worker_type = worker_data.get('type')
        if not worker_type:
            return
        worker_class = self.workers_type_dict.get(worker_type)
        if worker_class:
            return worker_class(worker_field, worker_data)


class QuerysetProcessorWorkersGeneric(QuerysetProcessorWorkersBase):
    def prepare(self, request):
        data = self.get_base_data(request)
        if not data:
            return
        for worker_field, worker_data in data.items():
            worker = self.create_worker_by_type(worker_field, worker_data)
            if worker:
                self.workers_objs.append(worker)


class QuerysetProcessorFiltersGeneric(QuerysetProcessorWorkersGeneric):
    workers = [FilterWorkerMinMax, FilterWorkerBool, FilterWorkerDatetimeMinMax]
    field = "filters"

    def apply(self, qs):
        for worker_obj in self.workers_objs:
            qs = worker_obj.apply(qs)
        return qs


class QuerysetProcessorSortersGeneric(QuerysetProcessorWorkersGeneric):
    workers = [SorterWorkerAscDesc, SorterWorkerTrueFalse]
    field = "sorters"

    def apply(self, qs):
        sorted_workers = sorted(self.workers_objs, key=lambda x: x.data.get('pose'))

        sorter_fieldnames = []
        for worker in sorted_workers:
            fild_name = worker.field
            value = worker.data.get('value')
            if value:
                fild_name = '-' + fild_name
            sorter_fieldnames.append(fild_name)
        if sorter_fieldnames:
            qs = qs.order_by(*sorter_fieldnames)
        return qs


class QuerysetProcessorSearchGeneric(QuerysetProcessorBase):
    field = "search"
    search_field = None

    def __init__(self, *args, **kwargs):
        self.data = None
        super().__init__(*args, **kwargs)

    def prepare(self, request):
        self.data = self.get_base_data(request)

    def apply(self, qs):
        if self.data:
            qs = self.search(qs)
        return qs

    def search(self, qs):
        assert self.search_field, (
                "'%s' should either include a `search_field` attribute, "
                "or override the `search()` method."
                % self.__class__.__name__
        )
        if self.data:
            qs = qs.filter(**{self.search_field + '__icontains': self.data})
        return qs


class QuerysetProcessorSearchFields(QuerysetProcessorBase):
    field = "search"
    search_fields = None

    def __init__(self, *args, **kwargs):
        self.data = None
        super().__init__(*args, **kwargs)

    def prepare(self, request):
        self.data = self.get_base_data(request)

    def apply(self, qs):
        if self.data:
            qs = self.search(qs)
        return qs

    def search(self, qs):
        assert self.search_fields and isinstance(self.search_fields, list), (
                "'%s' should either include a `search_fields` attribute, "
                "or override the `search()` method."
                % self.__class__.__name__
        )
        if self.data:
            qs = qs.filter(
                Q(**{f"{f}__contains": self.data for f in self.search_fields},
                  _connector=Q.OR)
            )
        return qs


class QuerysetProcessorIDGeneric(QuerysetProcessorBase):
    field = None
    search_field = None

    def __init__(self, *args, **kwargs):
        assert self.field, (
                "'%s' should either include a `field` attribute"
                % self.__class__.__name__
        )
        self.data = None
        super().__init__(*args, **kwargs)

    def prepare(self, request):
        self.data = self.get_base_data(request)

    def apply(self, qs):
        if self.data:
            qs = self.search(qs)
        return qs

    def search(self, qs):
        assert self.search_field, (
                "'%s' should either include a `search_field` attribute, "
                "or override the `search()` method."
                % self.__class__.__name__
        )
        if self.data:
            qs = qs.filter(**{self.search_field: self.data})
        return qs