from rest_framework.views import APIView


errr = {
    'emitter': 'advantage',  # кто создал ошибку
    'layer': 'view',  # слой - если во view, то
    'module': '',  #
    'service': '',  #
}


class ExampleApiView(APIView):

    def get(self, request):
        if request.data.get('foo') == 'bar':
            raise ValidationError('')
