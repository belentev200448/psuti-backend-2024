from abc import ABC, abstractmethod

from advantage.drf.func_validators.exceptions import SmartApiViewInitValidationException
from advantage.drf.func_validators.utils import is_iterable, is_serializer


class FuncValidatorBase(ABC):
    def __init__(self, method_name, signature, annotations, cls_path=None):
        self.method_name = method_name
        self.signature = signature
        self.annotations = annotations
        self.cls_path = cls_path

    def copy_init_objs(self):
        self.signature_params = self.signature.parameters.copy()
        self.annotations = self.annotations.copy()

    @abstractmethod
    def get_exception(self):
        pass

    @abstractmethod
    def check(self):
        pass


class SingHasArgGeneric(FuncValidatorBase):
    arg = None

    def get_arg(self):
        if self.arg is None:
            raise Exception(f'{self.__class__.__name__} must have "arg" class attr or override get_arg method')
        return self.arg

    def get_exception(self):
        return SmartApiViewInitValidationException(
            error_detail=f'{self.method_name} function must have a {self.get_arg()} argument',
            cls_path=self.cls_path,
        )

    def check(self):
        if not self.signature.parameters.get(self.get_arg()):
            raise self.get_exception()


class AnnotationsTypeGeneric(FuncValidatorBase):
    arg = None
    type = None

    def get_arg(self):
        if self.arg is None:
            raise Exception(f'{self.__class__.__name__} must have "arg" class attr or override get_arg method')
        return self.arg

    def get_type(self):
        if self.type is None:
            raise Exception(f'{self.__class__.__name__} must have "type" class attr or override get_type method')
        return self.type

    def get_exception(self):
        return SmartApiViewInitValidationException(
            error_detail=f'Argument "{self.get_arg()}" in function "{self.method_name}" must have a "{self.get_type()}" type',
            cls_path=self.cls_path,
        )

    def check(self):
        if self.annotations.get(self.get_arg()) is not self.get_type():
            raise self.get_exception()


class AnnotationsTypeUnionGeneric(FuncValidatorBase):
    arg = None
    types = None

    def get_arg(self):
        if self.arg is None:
            raise Exception(f'{self.__class__.__name__} must have "arg" class attr or override get_arg method')
        return self.arg

    def get_types(self):
        if self.types is None:
            raise Exception(f'{self.__class__.__name__} must have "types" class attr or override get_types method')
        if not is_iterable(self.types):
            raise Exception(f'Class attr "types" in {self.__class__.__name__} must be iterable')
        return self.types

    def get_exception(self):
        return SmartApiViewInitValidationException(
            error_detail=f'Cannot find valid type for "{self.get_arg()}" argument in function "{self.method_name}".',
            cls_path=self.cls_path,
        )

    def check(self):
        arg_obj = self.annotations.get(self.get_arg())
        for type_ in self.get_types():
            if arg_obj is type_:
                return
        raise self.get_exception()


class CheckSignAntMatchGeneric(FuncValidatorBase):
    exclude_args = None

    def get_exclude_args(self):
        if self.exclude_args is None:
            raise Exception(f'{self.__class__.__name__} must have "exclude_args" class attr or override get_exclude_args method')
        if not is_iterable(self.exclude_args):
            raise Exception(f'Class attr "exclude_args" in {self.__class__.__name__} must be iterable')
        return self.exclude_args

    def get_exception(self, param):
        return SmartApiViewInitValidationException(
            error_detail=f'Unable to find annotation for "{param}" argument in "{self.method_name}" function.',
            cls_path=self.cls_path,
        )

    def check(self):
        self.copy_init_objs()
        for e_arg in self.get_exclude_args():
            if self.annotations.get(e_arg):
                del self.annotations[e_arg]
            if self.signature_params.get(e_arg):
                del self.signature_params[e_arg]
        for param in self.signature_params.keys():
            param_annotation = self.annotations.get(param)
            if not param_annotation:
                raise self.get_exception(param)


class AntTypesNameHardCheck(FuncValidatorBase):
    """Проверяет, что определённые аргументы соответствуют определённому типу"""
    types = None

    def get_exception(self, arg_name):
        return SmartApiViewInitValidationException(
            error_detail=f'`{arg_name}` argument in "{self.method_name}" .',
            cls_path=self.cls_path,
        )

    def get_types(self):
        if self.types is None:
            raise Exception(f'{self.__class__.__name__} must have "types" class attr or override get_types method')
        if not isinstance(self.types, dict):
            raise Exception(f'Class attr "types" in {self.__class__.__name__} must be dict')
        return self.types

    def check(self):
        for check_type_func, args_names in self.get_types().items():
            for arg_name in args_names:
                ant_arg = self.annotations.get(arg_name)
                if ant_arg and not check_type_func(ant_arg):
                    raise self.get_exception(arg_name)


class AntTypeNameHardCheck(AntTypesNameHardCheck):
    """Проверяет, что для определённого типа существуют только определённые аргументы"""
    def check(self):
        self.copy_init_objs()
        for check_type_func, args_names in self.get_types().items():
            for arg_name in args_names:
                ant_arg = self.annotations.get(arg_name)
                if ant_arg and not check_type_func(ant_arg):
                    raise self.get_exception(arg_name)
