import httpx
from rest_framework.exceptions import ValidationError


def get_user_data(oauth_token: str):
    resp = httpx.get(f"https://www.googleapis.com/oauth2/v2/userinfo?access_token={oauth_token}")
    if not resp.is_success:
        raise ValidationError('Invalid google oauth code')
    data = resp.json()
    return data

test = {
  "family_name": "Netger",
  "name": "The Netger",
  "picture": "https://lh3.googleusercontent.com/a/ACg8ocJNb1fh-Ho6xuEt83tvoOtFTdzZa-R7Sc4xO9GmcqxWQA=s96-c",
  "locale": "ru",
  "email": "belentev200448@gmail.com",
  "given_name": "The",
  "id": "108397835578956954559",
  "verified_email": True
}