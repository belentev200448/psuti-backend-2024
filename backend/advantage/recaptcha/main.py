from collections.abc import Iterable
from typing import Union

from google.cloud import recaptchaenterprise_v1
from rest_framework.exceptions import ValidationError


Reasons = recaptchaenterprise_v1.AnnotateAssessmentRequest.Reason


class RecaptchaController:
    client = recaptchaenterprise_v1.RecaptchaEnterpriseServiceClient()
    site_key = None
    project_id = None

    def _get_site_key(self):
        assert self.site_key is not None, (f"{self.__class__.__name__}: `site_key` attribute must not be None"
                                           " or override `get_site_key` method")
        return self.site_key

    def _get_project_id(self):
        assert self.project_id is not None, (f"{self.__class__.__name__}: `project_id` attribute must not be None"
                                           " or override `get_project_id` method")
        return self.project_id

    def _get_project_name(self):
        return f"projects/{self._get_project_id()}"

    def _create_event(self):
        event = recaptchaenterprise_v1.Event()
        event.site_key = self._get_site_key()
        event.token = self.token
        event.expected_action = self.action
        return event

    def _create_assessment(self):
        assessment = recaptchaenterprise_v1.Assessment()
        assessment.event = self.event
        return assessment

    def _create_request(self):
        request = recaptchaenterprise_v1.CreateAssessmentRequest()
        request.assessment = self.assessment
        request.parent = self._get_project_name()
        return request

    def _make_request(self):
        return self.client.create_assessment(self.request)

    def _validate_response(self):
        if not self.response.token_properties.valid:
            raise ValidationError({"invalid_token": "The CreateAssessment call failed because the token was "
                                                    + "invalid for the following reasons: "
                                                    + str(self.response.token_properties.invalid_reason)}
                                  )
        if self.response.token_properties.action != self.action:
            raise ValidationError({"invalid_action": "The action attribute in your reCAPTCHA tag does"
                                                     + "not match the action you are expecting to score"})


    @property
    def score(self):
        return self.response.risk_analysis.score

    @property
    def assessment_name(self):
        return self.client.parse_assessment_path(self.response.name).get("assessment")

    def __init__(self, token: str, action: str):
        self.token = token
        self.action = action
        # recaptcha pipeline
        self.event = self._create_event()
        self.assessment = self._create_assessment()
        self.request = self._create_request()
        self.response = self._make_request()
        self._validate_response()

    def annotate(self, annotation, reasons):
        request = recaptchaenterprise_v1.AnnotateAssessmentRequest()
        request.name = self.assessment_name
        request.annotation = annotation
        request.reasons = reasons
        response = self.client.annotate_assessment(request)

    def annotate_success(self, reasons):
        annotation = recaptchaenterprise_v1.AnnotateAssessmentRequest.Annotation.LEGITIMATE
        self.annotate(annotation, reasons)

    def annotate_failure(self, reasons):
        annotation = recaptchaenterprise_v1.AnnotateAssessmentRequest.Annotation.FRAUDULENT
        self.annotate(annotation, reasons)


class RecaptchaVirtualSpace:
    platforms: dict[str, type[RecaptchaController]] = None
    min_success_score: Union[int, float] = 0

    @classmethod
    def _get_platforms(cls):
        assert cls.platforms is not None, (f"{cls.__class__.__name__}: `platforms` attribute must not be None"
                                            " or override `_get_platforms` method")
        return cls.platforms

    @classmethod
    def get_platforms_choices(cls):
        platforms = cls._get_platforms()
        return (
            (p, p) for p in platforms
        )

    def get_recaptcha_controller_cls(self, platform: str):
        controller = self._get_platforms().get(platform)
        if not controller:
            raise ValidationError('Recaptcha platform not found')
        return controller

    def validate_user(self, controller: RecaptchaController):
        if controller.score < self.min_success_score:
            raise ValidationError('Recaptcha score too low')

    def get_recaptcha_controller(self, platform: str, token: str, action: str):
        controller_cls = self.get_recaptcha_controller_cls(platform)
        controller = controller_cls(token, action)
        self.validate_user(controller)
        return controller

    @staticmethod
    def make_iterable(obj):
        if not isinstance(obj, Iterable):
            return [obj] if obj else [Reasons.REASON_UNSPECIFIED]
        else:
            return obj if obj else [Reasons.REASON_UNSPECIFIED]

    def __init__(self,
                 platform: str,
                 token: str,
                 action: str,
                 on_success: list[int],
                 on_error: list[int],
                 view,):
        self.controller: RecaptchaController = self.get_recaptcha_controller(platform, token, action)

        self.on_success = self.make_iterable(on_success)
        self.on_error = self.make_iterable(on_error)
        self.view = view

    def __call__(self, *args, **kwargs):
        try:
            response = self.view(*args, **kwargs)
        except ValidationError as e:
            # self.controller.annotate_failure(self.on_error)
            raise e
        else:
            pass
            # self.controller.annotate_success(self.on_success)
        return response
